package com.example.android_app.ui.dao

import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import android.widget.Toast
import com.example.android_app.ui.bean.Devices
import com.google.firebase.firestore.FirebaseFirestore

class DeviceDao(
    private val context: Context,
) {

    constructor() :  this(context = null!!)
    // add user and like parameter un hashmapofuser
    fun addDevice(Device: Devices, fb_fs: FirebaseFirestore) {

        fb_fs.collection("Device")
            .add(hashMapOf(
                "id" to Device.id,
                "id_api" to Device.id_api,
                "name" to Device.name,
                "temperature" to Device.temperature,
                "humidity" to Device.humidity,
                "Co2" to Device.Co2,
            ))
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
                Toast.makeText(context, "Device added", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
                Toast.makeText(context, "Device not added", Toast.LENGTH_SHORT).show()
            }

    }
    fun getDevice() {
    }
    fun deleteDevice() {
    }

}