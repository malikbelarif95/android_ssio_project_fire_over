package com.example.android_app.ui.bean

enum class Status {
    ONLINE,
    OFFLINE
}