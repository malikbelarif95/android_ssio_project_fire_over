package com.example.android_app.ui.devices

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.android_app.databinding.FragmentDevicesBinding

class DevicesFragment : Fragment() {
    private var _binding: FragmentDevicesBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val deviceViewModel = ViewModelProvider(this).get(DeviceViewModel::class.java)
        _binding = FragmentDevicesBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.textDevices
        deviceViewModel.text.observe(viewLifecycleOwner) {
            textView.text = it
        }
        return root

    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}