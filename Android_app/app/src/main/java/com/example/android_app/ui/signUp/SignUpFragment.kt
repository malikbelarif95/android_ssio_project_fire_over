package com.example.android_app.ui.signUp
import android.annotation.SuppressLint
import com.example.android_app.ui.pswSecurity.pswSecurity
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.android_app.R
import com.example.android_app.databinding.AdduserfregmentBinding
import com.example.android_app.ui.bean.User
import com.example.android_app.ui.dao.UserDao
import com.google.firebase.firestore.ktx.firestore
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import java.util.Base64

import com.google.firebase.ktx.Firebase

class SignUpFragment: Fragment() {
    private val pswSecure = pswSecurity()
    private var _binding: AdduserfregmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var userDao: UserDao
    private val is_valid_email_reges = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    // mutable memeber by boolean to enable button only when all fields are valid
    private var is_valid_name = false
    private var is_valid_email = false
    private var is_valid_password = false
    private var is_valid_confirm_password = false
    private var encodingkey = "afus%deg%fus%wala%anefk%afus"

    fun is_all_valide(is_valid_name: Boolean, is_valid_email: Boolean, is_valid_password: Boolean, is_valid_confirm_password: Boolean): Boolean {
        return is_valid_name && is_valid_email && is_valid_password && is_valid_confirm_password
    }

    @SuppressLint("SuspiciousIndentation")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View {
        val signUpViewModel =
            ViewModelProvider(this).get(SignUpViewModel::class.java)
        _binding = AdduserfregmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val buttonSignUp: Button = binding.buttonRegister
        // fin vieuw by ID button
        val btn = buttonSignUp.findViewById<Button>(R.id.buttonRegister)
        btn.isEnabled = false

        val editTextName: EditText = binding.editTextName

        val editTextEmail: EditText = binding.editTextEmail

        val editTextPassword: EditText = binding.editTextPassword

        val editTextConfirmPassword: EditText = binding.editTextConfirmPassword

            // set on focus change listener to validate fields
            editTextName.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {
                    if (editTextName.text.toString() == "") {
                        editTextName.error = "Name is required"
                    } else {
                        editTextName.error = null
                        is_valid_name = true
                        if (is_all_valide(is_valid_name, is_valid_email, is_valid_password, is_valid_confirm_password)) {
                            btn.isEnabled = true
                            // set bacckgroungHint to green
                            btn.setBackgroundColor(Color.GREEN)
                        }
                    }
                }

                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                    // TODO Auto-generated method stub
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    // TODO Auto-generated method stub
                }
            })


            editTextEmail.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {
                    if (editTextEmail.text.toString() == "") {
                        editTextEmail.error = "Email is required"
                    } else if (!editTextEmail.text.toString().matches(is_valid_email_reges.toRegex())) {
                        editTextEmail.error = "Email is not valid"
                    } else {
                        editTextEmail.error = null
                        is_valid_email = true
                        if (is_all_valide(is_valid_name, is_valid_email, is_valid_password, is_valid_confirm_password)) {
                            btn.isEnabled = true
                            btn.setBackgroundColor(Color.GREEN)
                        }
                    }
                }

                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                    // TODO Auto-generated method stub
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    // TODO Auto-generated method stub
                }
            })

            editTextPassword.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {
                    if (editTextPassword.text.toString() == "") {
                        editTextPassword.error = "Password is required"
                    } else if (editTextPassword.text.toString().length < 6) {
                        editTextPassword.error = "Password must be at least 6 characters"
                    } else {
                        editTextPassword.error = null
                        is_valid_password = true
                        if (is_all_valide(is_valid_name, is_valid_email, is_valid_password, is_valid_confirm_password)) {
                            btn.isEnabled = true
                            btn.setBackgroundColor(Color.GREEN)
                        }
                    }
                }

                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                    // TODO Auto-generated method stub
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    // TODO Auto-generated method stub
                }
            })

            editTextConfirmPassword.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {
                    if (editTextConfirmPassword.text.toString() == "") {
                        editTextConfirmPassword.error = "Confirm Password is required"
                    } else if (editTextConfirmPassword.text.toString() != editTextPassword.text.toString()) {
                        editTextConfirmPassword.error = "Password does not match"
                    } else {
                        editTextConfirmPassword.error = null
                        is_valid_confirm_password = true
                        if (is_all_valide(is_valid_name, is_valid_email, is_valid_password, is_valid_confirm_password)) {
                            btn.isEnabled = true
                            btn.setBackgroundColor(Color.GREEN)

                        }
                    }
                }

                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                    // TODO Auto-generated method stub
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    // TODO Auto-generated method stub
                }
            })


            // set on click listener to enable button only whem n all fields are valid


            buttonSignUp.setOnClickListener {
                val textConfirmPassword =
                    editTextConfirmPassword.findViewById<EditText>(R.id.editTextConfirmPassword).text.toString()
                val textPassword =
                    editTextPassword.findViewById<EditText>(R.id.editTextPassword).text.toString()
                val textEmail = editTextEmail.findViewById<EditText>(R.id.editTextEmail).text.toString()
                val textName = editTextName.findViewById<EditText>(R.id.editTextName).text.toString()

                Toast.makeText(context, textName, Toast.LENGTH_SHORT).show()
                Toast.makeText(context, textEmail, Toast.LENGTH_SHORT).show()
                Toast.makeText(context, textPassword, Toast.LENGTH_SHORT).show()
                Toast.makeText(context, textConfirmPassword, Toast.LENGTH_SHORT).show()

                val db = Firebase.firestore
                userDao = UserDao(context = requireContext())
                // Create a new user with a first and last name
                val user = User(textName, textEmail, pswSecure.encryptString(textPassword))

                if (
                    editTextConfirmPassword.text.toString() == textPassword &&
                    textName != "" &&
                    textEmail != "" &&
                    textPassword != ""
                ) {
                    userDao.addUser(user, db)

                    // do a function with clear and error = null
                    editTextEmail.text.clear()
                    editTextName.text.clear()
                    editTextPassword.text.clear()
                    editTextConfirmPassword.text.clear()

                    editTextName.error = null
                    editTextEmail.error = null
                    editTextPassword.error = null
                    editTextConfirmPassword.error = null

                } else {
                    Toast.makeText(context, "Please check your input", Toast.LENGTH_SHORT).show()
                }
            }

            return root
        }

        override fun onDestroyView() {
            super.onDestroyView()
            _binding = null
        }
}