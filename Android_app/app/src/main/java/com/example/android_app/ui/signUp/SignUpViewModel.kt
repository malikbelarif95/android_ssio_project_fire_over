package com.example.android_app.ui.signUp

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android_app.ui.bean.User
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import java.security.AccessController.getContext

class SignUpViewModel: ViewModel() {

    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val confirmPassword = MutableLiveData<String>()
    val name = MutableLiveData<String>()


    fun onSignUpClicked(user: User) {
        val context = getContext() as Context
       try {
           // get cuurent context


           Toast.makeText(context, "Sign Up", Toast.LENGTH_SHORT).show()
           if ((password.value != confirmPassword.value) && (password.value != null) && (confirmPassword.value != null) && (name.value != null) && (email.value != null)) {
               val options = FirebaseOptions.Builder()
                   .setProjectId("fireover-c4d26")
                   .setDatabaseUrl("https://fireover-c4d26-default-rtdb.europe-west1.firebasedatabase.app/")
                   .build()
               FirebaseApp.initializeApp(context, options, "FireOver")
               //val db = FirebaseFirestore.getInstance(FirebaseApp.getInstance("FireOver"))

               /*
               val myRef = db.collection("User")
               myRef.document(user.name).set(user)
               Toast.makeText(context, "User "+ name.value +" is created", Toast.LENGTH_SHORT).show()
           } else {
               Toast.makeText(context, "Passwords do not match", Toast.LENGTH_SHORT).show()
           }

       }catch (e: Exception){
           Toast.makeText(context, "Error: "+e.message, Toast.LENGTH_SHORT).show()
       }finally {
              Toast.makeText(context, "Connected to Firebase", Toast.LENGTH_SHORT).show()
       }
       */


/*
        viewModelScope.launch {
            // Get the email, password, and confirm password from the MutableLiveData objects

            // If the password and confirm password do not match, return
            if (password != confirmPassword) {
                *//* Toast an error *//*
                Toast.makeText(context, "Passwords do not match", Toast.LENGTH_SHORT).show()
                return@launch
            }

            // Check if a user with the given email already exists in the database
            val existingUser = connectionDb.userDao.getUserByEmail(email)


            // If a user with the given email already exists, return
            if (existingUser != null) {
                *//* Toast an email already exists *//*
                Toast.makeText(context, "Email already exists", Toast.LENGTH_SHORT).show()
                return@launch
            }

            // Create a new user object and insert it into the database
            val newUser = User(
                id = "",
                name = name.value!!,
                email = email.value!!,
                password = password.value!!
            )
            connectionDb.userDao.addUser(newUser)

            Toast.makeText(context, "User "+ name.value +" is created", Toast.LENGTH_SHORT).show()
        }
    }*/
               //}
           }
       }catch (e: Exception){
           Toast.makeText(context, "Error: "+e.message, Toast.LENGTH_SHORT).show()
         }finally {
              Toast.makeText(context, "Connected to Firebase", Toast.LENGTH_SHORT).show()

       }
    }
}
