package com.example.android_app.ui.dao

import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import android.widget.Toast
import com.example.android_app.ui.bean.User
import com.google.firebase.firestore.FirebaseFirestore


class UserDao(
    private val context: Context,
) {
    constructor() :  this(context = null!!)
    // add user and like parameter un hashmapofuser
    fun addUser(user: User, fb_fs: FirebaseFirestore) {

        fb_fs.collection("User")
            .add(hashMapOf(
                "name" to user.name,
                "email" to user.email,
                "password" to user.password
            ))
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
                Toast.makeText(context, "User added", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
    }

    fun getUser(fb_fs: FirebaseFirestore) {
        fb_fs.collection("User")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    Log.d(TAG, "${document.id} => ${document.data}")
                }
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }
    fun getUserbyEmail(fb_fs: FirebaseFirestore, email: String) {
        fb_fs.collection("User")
            .whereEqualTo("email", email)
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    Log.d(TAG, "${document.id} => ${document.data}")
                }
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }

    fun deleteUser(fb_fs: FirebaseFirestore) {
        fb_fs.collection("User")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    Log.d(TAG, "${document.id} => ${document.data}")
                    fb_fs.collection("User").document(document.id).delete()
                }
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }

}

