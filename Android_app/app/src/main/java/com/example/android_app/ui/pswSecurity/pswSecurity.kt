package com.example.android_app.ui.pswSecurity

import android.os.Build
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

class pswSecurity {
    // constructor

    private val encodingkey = "afus%deg%fus%wala%anefk%afus"
    fun encryptString(text: String): String {
        val cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
        val paddedKey = encodingkey.padEnd(32, '0').substring(0, 32) // Pad key to 32 bytes (256 bits)
        val secretKey = SecretKeySpec(paddedKey.toByteArray(), "AES")
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        val encrypted = cipher.doFinal(text.toByteArray(Charsets.UTF_8))
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Base64.getEncoder().encodeToString(encrypted)
        } else {
            TODO("VERSION.SDK_INT < O")
        }
    }

    fun decryptString(text: String): String {
        val cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
        val paddedKey = encodingkey.padEnd(32, '0').substring(0, 32) // Pad key to 32 bytes (256 bits)
        val secretKey = SecretKeySpec(paddedKey.toByteArray(), "AES")
        cipher.init(Cipher.DECRYPT_MODE, secretKey)
        val encryptedBytes = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Base64.getDecoder().decode(text)
        } else {
            TODO("VERSION.SDK_INT < O")
        }
        val decryptedBytes = cipher.doFinal(encryptedBytes)
        return String(decryptedBytes, Charsets.UTF_8)
    }
}