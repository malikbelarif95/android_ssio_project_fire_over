package com.example.android_app.ui.devices

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DeviceViewModel: ViewModel() {
    private val _text = MutableLiveData<String>().apply {
        value = "This is devices Fragment"
    }
    val text : LiveData<String> = _text
}