package com.example.android_app.ui.bean

data class Devices(
    val id : String,
    val name : String,
    val type : String,
    val status : Status,
    val temperature : String,
    val humidity : String,
    val Latitude : String,
    val Longitude : String,
    val Co2 : String,
    val id_api : String,
)
